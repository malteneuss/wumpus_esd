% Configure the indiGolog libraries.
% These options are globally available through 'get_option(Name, Value)'
% To include this file, use the ':- include(FileName)' directive, not 'use_module' or 'consult'.

%Global

% Set verbosity of debug messages
% The higher the level, the more verbose the messages are.
% You can print message by using
% 'report_messagage(system(N), Message)' where 'N' is the highest
% level at which 'Message' is printed.
:- set_option(debug_level, 2).



% Environment manager

% The environment manager wants 'signal' or 'thread'
% No idea why.
:- set_option(type_em, signal).

% The environment manager wants to know at which ip:port it should be listening.
server_host('localhost').
server_port(_).



%Device Manager

% The 'virtual_wumpus' device manager uses 'wumpus_config/5' to create
% the wumpus environment.
% wumpus_config(ProgramInterpreter, CaveSize, PitProbabilityInPercent, NumberOfGoldPieces, ScenarioID)
:- dynamic wumpus_config/5.
wumpus_config(indigolog(default), 4, 20, 2, random). %Use as default.



%IndiGolog program interpreter

%Set time to wait after each executed action.
%Apparantly system crashes without it.
:- set_option(wait_step, 0.2).
