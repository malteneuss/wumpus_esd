:- module(deterministicControl, [
  proc/2
]).
% Similiar to the ES deterministic golog program
proc(main, deterministic).

proc(deterministic,[
  #senseBreeze
 ,#senseStench
 ,if(know( ?([R1 in rooms, R2 in rooms],
              adj(R1, R2) & agentLoc(R1) & wumpusLoc(R2))
          ),
	  #shoot
	 ,[]
  )
 ,while(know(inCave),[
    exploreLine(#right)	% move right as long as possible
	 ,if(know(inCave), [
      nextLine(#left)
		 ,if(know(?([R3 in rooms, R4 in rooms],
              adj(#left, R3, R4) & agentLoc(R3))
              ), [
        exploreLine(#left)
		   ,if(know(inCave),
			    nextLine(#right)
			   ,[]
        )
      ],[])
    ], [])
  ])
]) :- !, print_message(debug(program), "Start deterministic").



proc(kbatExample,
   prioritized_interrupts(
         [interrupt(know(isGold(locA)), [#pickGold]),
	  interrupt(inCave=true, [#senseStench,#senseBreeze,#senseGlitter,
				wndet(newRandomWalk, [goto(loc(1,1))])])
         ])  % END OF INTERRUPTS
).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Auxiliary Programs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

proc(afterMove,[
  if(know(?([R1 in rooms, R2 in rooms],
            adj(R1, R2) & agentLoc(R1) & wumpusLoc(R2) & hasArrow)),[
    search([
      pi(dir, dirs, [
        ?(know(?([R3 in rooms, R4 in rooms],
               adj(dir, R3, R4) & agentLoc(R3) & wumpusLoc(R4))))
       ,shoot(dir)
      ])
    ])
	], []),
  if(know(?([R5 in rooms], agentLoc(R5) & goldLoc(R5))),[
    #pickGold
	 ,runHome
	],[])
]) :- !, print_message(debug(program), "Start afterMove").

proc(nextLine(Dir),[
  while(isSafe(Dir))
  while(-know(?([R1 in rooms, R2 in rooms],
                adj(#up, R1, R2) & agentLoc(R1) %& safe(R2)
                & pitSafe(R2) & (-wumpusAlive v wumpusSafe(R2))
                ))
        %& know(?([R3 in rooms, R4 in rooms],
         %       adj(Dir, R3, R4) & agentLoc(R3) %& safe(R4)
          %      & pitSafe(R4) & (-wumpusAlive v wumpusSafe(R4))
           %     )) ,
	  move(Dir)
	),
   %,break
  if(know(?([R5 in rooms, R6 in rooms],
              adj(#up, R5, R6) & agentLoc(R5) %& safe(R6)
              & pitSafe(R6) & (-wumpusAlive v wumpusSafe(R6))
                          )) ,[
      fullMove(#up)
	   ,afterMove
	  ],
    runHome
  )
]) :- def(!, print_message(debug(program), "Start nextLine"), write(Dir), nl.


def(iSafe(Dir), ..).
proc(exploreLine(Dir),[
  while(know(?([R1 in rooms, R2 in rooms],
                adj(Dir, R1, R2) & agentLoc(R1) & inCave %& safe(R2)
                & pitSafe(R2) & (-wumpusAlive v wumpusSafe(R2))
                )),[
    fullMove(Dir)
	 ,afterMove
	])
]) :- !, print_message(debug(program), "Start exploreLine"), write(Dir), nl.


proc(runHome,[
  while(-know(agentLoc(#room(1,1))),
	  search(
      ndet([
        ?(know(?([R1 in rooms, R2 in rooms],
                adj(#left, R1, R2) & agentLoc(R1) %& safe(R2)
                & pitSafe(R2) & (-wumpusAlive v wumpusSafe(R2))
                )))
			 ,move(#left)
      ],[
        ?(know(?([R3 in rooms, R4 in rooms],
                adj(#down, R3, R4) & agentLoc(R3) %& safe(R4)
                & pitSafe(R4) & (-wumpusAlive v wumpusSafe(R4))
                )))
			 ,move(#down)
      ])
		)
	)
 ,#climb
]) :- !, print_message(debug(program), "Start runHome").


proc(turn(Dir), [
  search([
    star(#turnRight,4)
   ,?(looking(Dir))
  ])
]) :- !, print_message(debug(program), "Start turn"), write(Dir), nl.

proc(move(Dir),[
  turn(Dir)
 ,#move
]).

proc(shoot(Dir),[
  turn(Dir)
 ,#shoot
]).

%proc(turnToWumpus,
%     search([wndet([?(rightR(locA,locW)), star(#turnRight, 4), ?(dirA=right)],
%		   wndet([?(downR(locA, locW)), star(#turnRight, 4), ?(dirA=down)],
%			 wndet([?(leftR(locA, locW)), star(#turnRight, 4), ?(dirA=left)],
%			       [?(upR(locA, locW)), star(#turnRight, 4), ?(dirA=up)])))
%	    ])).

proc(fullMove(Dir), [
  move(Dir)
 ,#senseStench
 ,#senseBreeze
 ,#senseGlitter
]) :- !, print_message(debug(program), "Start fullMove"), write(Dir), nl.

proc(goto(TargetRoom), [
  wndet([
    ?(?([R in rooms], adj(#right, R, TargetRoom) & agentLoc(R))), move(#right)]
   ,wndet([
      ?(?([R2 in rooms], adj(#down, R2, TargetRoom) & agentLoc(R2))), move(#down)]
     ,wndet([
        ?(?([R3 in rooms], adj(#left, R3, TargetRoom) & agentLoc(R3))), move(#left)]
        ,[?(?([R4 in rooms], adj(#up, R4, TargetRoom) & agentLoc(R4))), move(#up)]
      )
    )
  )
]) :- !, print_message(debug(program), "Start goto"), write(TargetRoom), nl.

proc(fullGoto(TargetRoom), [
  goto(TargetRoom)
 ,#senseStench
 ,#senseBreeze
 ,#senseGlitter
]).

%proc(goto1step(Loc),
%	  pi(x,dir,[move(x),?(locA=Loc)])
%).
