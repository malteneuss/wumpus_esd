:- module(simpleWalkControl,[
  proc/2
]).

proc(main,
  [ #turnRight
  , #turnRight
  , #turnRight
  , #shoot
  , senseAndMoveFwd
  , senseAndMoveFwd
  , #turnRight
  , senseAndMoveFwd
  , #turnRight
  , senseAndMoveFwd
  , senseAndMoveFwd
  , #turnRight
  , senseAndMoveFwd
  , #climb
  ]) :- print_message(debug(main), 'IndiGolog-main was used').


proc(senseAndMoveFwd,
  [ #senseBreeze
  , #senseStench
  , #senseGlitter
  , #move
  ]).
