:- module(trivialControl,[
  proc/2
]).

proc(main,[
  #shoot
 ,#senseBreeze
 ,#senseStench
 ,#move
 ,#senseGlitter
 ,if(?([R in rooms], agentLoc(R) & goldLoc(R)),
    [#pickGold]
   ,?(true)
  )
 ,if(hasArrow,
   #shoot,
   []
  )
 ,#turnRight
 ,#turnRight
 ,#move
 ,#climb
]) :- print_message(debug(main), 'IndiGolog-main was used').

