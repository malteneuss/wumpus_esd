%#! /bin/env swipl -s

%  AUTHOR    : Malte Neuss @rwth-aachen.de
%  TESTED    : SWI-Prolog 7.1.37 under Ubuntu Linux 14.04.04
%
% DESCRIPTION:
% This file loads all necessary files into one global namespace.



:- multifile(
  [ sensing/2
  , system_action/1
  ]).

:- use_module(
	[ 'lib/logic/evalEsdInterface' % Load logical expression evaluator
  , 'lib/logic/esdRepresentation'
  % , 'agentControl/trivialControl'
  , 'agentControl/simpleWalkControl'
  % , 'agentControl/deterministiControl'
  ]).


% Unfortunately the transition system in IndiGolog requires
% a lot of predicates from other files in the global namespace of this file
% because it is old and doesn't use modules.
% All ':- include ...' directives load predicates from other file
% as if it was defined in this file.

% Global include code and Prolog init
:- include('../../lib/systemvar').

% Alpha* path finding for IndiGolog
:- consult('../../lib/alpha_star').

% Load IndiGolog program interpreter
:- consult('../../Interpreters/indigolog').

% Load Environment manager. It creates and communicates with
% the virtual wumpus environment.
:- consult('../../Env/env_man').

% Consult reusable list of device managers=environments.
% Has to be 'consult', 'include' doesn't work.
:- ['../../Env/dev_managers'].

% Can computes statistical numbers for collection of executions
:- consult('lib/benchmark/statwumpus').

% Workaround for indigolog overriding module predicates.
% We override it ourself.
sensing(A, B) :- evalEsdInterface:sensing(A, B).
system_action(A) :- evalEsdInterface:system_action(A).

% Provide esd reasoner for evaluation queryies.
eval(Query, H, Answer) :- evalEsdInterface:eval(Query, H, Answer).



% Load configuration for framework.
%:- include('config').
%:- include(indiGologAgent).






:- dynamic wumpus_config/5.
% Default configuration creates a random 3x3 map:
wumpus_config(ExecutionID, CaveSize, ProbabilityForPits, GoldCountOnMap, MapID) :- true
  % Every single execution, called 'run' in IndiGolog, can have its own name/Id
  , ExecutionID = indigolog(default)
  % The cave will have a 3x3 dimension
  , CaveSize = 3
  , ProbabilityForPits = 10 % In Percent
  , GoldCountOnMap = 1 % How much gold will be on the map
    % If MapId, called 'scenario' in IndiGolog, is not 'random'
    % but e.g. 'myOwnMap' then the values for
    % CaveSize etc are ignored and device_manager will
    % look for a wumpus_run/8 predicate with scenario name 'myOwnMap'
  %, MapID = random
  , MapID = test_sample_3(1) % use a specific map from the benchmark sample
  .

% Set indigolog interpreter options
:- set_option(debug_level, 4).
:- set_option(wait_step, 0.4).

% Set swi-prolog environment option to allow more RAM
:- set_prolog_stack(global, limit(2 000 000 000)).
:- set_prolog_stack(trail,  limit(2 000 000 000)).
:- set_prolog_stack(local,  limit(2 000 000 000)).



% Main uses the default random 3x3 configuration.
main :- !
  , shell('cd WumpusAppletKBAT; ./startWumpusGUI &')
  % , shell('cd WumpusAppletES; ./startWumpusGUI &')
  % , shell('cd ..')
  , indigolog
  .



benchmark :-
  compile('lib/benchmark/wumpus3x3GryslaSample') % Loads and overrides the wumpus_run/6 configurations
 ,findall(TempMapID, wumpus_run(TempMapID,_,_,_,_,_), MapIDs)
 ,forall(member(MapID, MapIDs),
   (MapID =.. [_Name, Number]
   ,ExecutionID =.. [benchmarked, Number] %
   ,benchmarkMap(ExecutionID, MapID) % If successful result will be added to 'logwumpus.pl'
   ))
 ,shell('mv logwumpus.pl benchmarkResult.pl')
 ,compile(benchmarkResult) % Loads and overrides the wumpus_run/6 configurations
  % Compute statistics from all successful execution that have a 'benchmark(_)' name
 ,get_stat_result(benchmarked(_), [_, _, _], Statistics)
 ,open('benchmarkResult.pl', append, Stream)
 ,write(Stream, statistic(Statistics)), write(Stream, '. \n') % Append into file.
 ,close(Stream)
.


% Performs one execution on a map referenced by 'MapID'.
% ExecutionID can be later used to find the log for this very execution.
benchmarkMap(ExecutionID, MapID) :-
  report_message(debug(benchmark), "Benchmark for Map started")
 ,write(MapID), write(' ExecutionID: '), write(ExecutionID), nl
 ,retractall(wumpus_config(_,_,_,_,_))
 %,shell('killall java') % Close Wumpus Applet if still running
 ,shell('cd WumpusAppletKBAT; ./startWumpusGUI &')
  % Only MapID and ExecutionID are important for next execution
 ,assert(wumpus_config(ExecutionID, notR, el3, vant, MapID))
  % Stop benchmark if execution take more than 240 seconds.
 ,catch(call_with_time_limit(240, indigolog), Exception, stopBenchmarkMap(Exception))
 ,shell('killall java') % Close Wumpus Applet if still running
.


stopBenchmarkMap(Exception) :-
  report_message(debug(benchmark), "Benchmark was aborted")
 ,write(Exception), nl
 ,finalizeEM % Shutdown Environment manager and wumpus device
 ,shell('killall clasp')
.




% This is used by the environment manager to create a terminal command
% that loads device managers in different processes.
% The instantiated device-manager/environment communicates with us
% via TCP/IP. We are the environment-manager and have to tell the
% environment on which IP/Port they can reach us.
% To create the environment via the terminal we reuse the command-builder from 'dev_managers'.
load_device(Environment, TerminalCommand, Address) :-
  EnvironmentManagerIP = null
 ,EnvironmentManagerPort = null
 ,(var(Address) -> Address = [EnvironmentManagerIP, EnvironmentManagerPort] ; true)
 ,Environment = virtual_wumpus
 %,Environment = virtual_wumpus
 ,PlattformType = swi % IndiGolog only supports swi anymore.
  % The device manager knows how to create the terminal command to create the wumpus environment
 ,device_manager(Environment, PlattformType, TerminalCommand, Address)
.
server_host('localhost'). % The environment manager runs on own computer.
server_port(_).



% The environment manager needs to know what low-level code to submit
% to a device manager.
% We have to map our action standard names  to the names that are used
% by the default wumpus environment 'virtual_wumpus'.
how_to_execute(EsdAction, virtual_wumpus, EnvironmentAction) :-
  member((EsdAction, EnvironmentAction), [
    (#climb, climb)
   ,(#pickGold, pickGold)
   ,(#shoot, shootFwd)
   ,(#move, moveFwd)
   ,(#turnRight, turn)
   ,(#senseBreeze, senseBreeze)
   ,(#senseGlitter, senseGold)
   ,(#senseStench, smell)
  ]), !.

how_to_execute(Action, virtual_wumpus, Action).

how_to_execute(Action, virtual_wumpus_silent, Action).



translateExogAction(scream, #exogWumpusScream).
%translateExogAction(CodeAction, Action) :-  actionNum(Action, CodeAction).
translateSensing(_, SensorValue, SensorEsdValue) :-
  (SensorValue == 1 -> SensorEsdValue == true ; SensorEsdValue == false)
.






