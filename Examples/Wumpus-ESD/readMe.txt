This folder holds the axiomatization of the Wumpus World.
AUTHOR    : Malte Neuss @RWTH-Aachen
email     : 78233-malteneuss@users.noreply.gitlab.com
Tested with SWI-Prolog 6.6.6 under Ubuntu Linux 14.04.2

Provided versions of Potassco-Tools
gringo v. 3
claspD v. 1.1.4
clasp v. 3.1.1



Loops.pl and loopsBat.pl contain the exact same data.
You need one copy for every module that uses it and has 
a dependency to another module that uses it too, otherwise i won't compile.



