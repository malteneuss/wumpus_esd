:- module(functionalStyle, [
  identity/2
 ,appendArgs/3
 ,compose/3
 ,compose/4
 ,compose/5
 ,compose/6
 ,filter/3
 ,filterArgs/3
 ,foldl/3
 ,mapArgs/3
 ,stringlist_concat/2
 ,insertInBetween/4
]).


:- use_module(library(apply)).

:- meta_predicate
  compose(2, 2, *, *)
 ,compose(2, 2, 2, *, *)
 ,compose(2, 2, 2, 2, *, *)
 ,filter(2, *, *)
 ,filterArgs(1, *, *)
 ,foldl(3, *, *)
 ,mapArgs(2, *, *)
.




insertInBetween(MiddleString, LeftString, RightString, ResultString) :-
  string_concat(LeftString, MiddleString, LeftMiddle)
 ,string_concat(LeftMiddle, RightString, ResultString)
.

stringlist_concat([], "").
stringlist_concat([S |Tail], Result) :-
  stringlist_concat(Tail, TailString)
 ,string_concat(S, TailString, Result)
.



% compose/3 %!!!Is broken due to module system, cannot export as meta_predicate!!!
% compose(ListOfFunctions, Argument, Result)
% Apply successively functions of the list to the 'Argument' to
% achive a point-free style known from functional programming.
% e.g. to achieve 'g(x, Temp), f(Temp, Result)' like 'f(g(x))'
% you can use 'compose([g, f],x, Result)' instead.
% Note that f, g must accept an argument and return a result as their last two
% parameters but can have more partially applied parameters before those as well.
compose([], Arg, Arg).

compose([Function |List], Arg, Result) :-
  call(Function, Arg, TempResult) %Get a binding for 'TempResult'
 ,compose(List, TempResult, Result). %Use 'TempResult' as argument for the next function

%Use these as workaround for compose/3.
compose(Function1, Function2, Arg, Result) :-
  call(Function1, Arg, Temp1) %Get a binding for 'Temp'
 ,call(Function2, Temp1, Result)
.

compose(F1, F2, F3, Arg, Result) :-
  call(F1, Arg, Temp1) %Get a binding for 'Temp'
 ,call(F2, Temp1, Temp2)
 ,call(F3, Temp2, Result)
.

compose(F1, F2, F3, F4, Arg, Result) :-
  call(F1, Arg, Temp1) %Get a binding for 'Temp'
 ,call(F2, Temp1, Temp2)
 ,call(F3, Temp2, Temp3)
 ,call(F4, Temp3, Result)
.



% Add a list of arguments to the end of a predicate.
appendArgs(Module:Predicate, Arguments, Module:PredicateWithArguments ) :- !,
  appendArgs(Predicate, Arguments, PredicateWithArguments).

appendArgs(Predicate, Arguments, PredicateWithArguments) :-
  Predicate =.. PredicateAsList %Function could have partially applied parameters
 ,append(PredicateAsList, Arguments, PredicateWithArgumentsAsList)
 ,PredicateWithArguments =.. PredicateWithArgumentsAsList
.



% identity/2
% identity(Argument, Argument)
% Succeeds if both arguments are one and the same. Use it as the identity function.
identity(Argument1, Argument2) :- Argument1 == Argument2.



% filter/3
% filter(Predicate, ListIn, FilteredList)
% Applies the 'Predicate' to each element in 'ListIn' and only keeps those elements who pass.
filter(Predicate, List, FilteredList) :- include(Predicate, List, FilteredList).



% filterArgs/3
% filterArgs(Predicate, Term, FilteredTerm)
% Applies the 'Predicate' to each argument of 'Term' and only keeps those arguments who pass.
filterArgs(Predicate, Term, FilteredTerm) :-
  Term=.. [Functor |Arguments]
 ,filter(Predicate, Arguments, FilteredArguments)
 ,FilteredTerm =.. [Functor | FilteredArguments]
.



% mapArgs/3
% mapArgs(FunctionPredicate, Term, TermWithMappedArgs)
% Replace all arguments in a term with the result of passing those through
% FunctionPredicate/2.
mapArgs(Predicate, Term, ResultTerm) :-
  Term=.. [Functor |Arguments]
 ,maplist(Predicate, Arguments, MappedArguments)
 ,ResultTerm =.. [Functor | MappedArguments]
.



% foldl/3
% Like foldl/4, but without a starting value accumulator.
% So it doesn't work with an empty list!
foldl(Predicate, [FirstItem |SubL], FoldedResult) :- !
  , apply:foldl(Predicate, SubL, FirstItem, FoldedResult).
