:- use_module([
  'lib/esdRepresentation'
 ,agent
]).

insertTestData :-
  assert(agent:cachedDomain(testDomain, [#a,#b]))
.

removeTestData :-
  retractall(agent:cachedDomain(_,))
.



:- begin_tests(grounding, [
  setup(insertTestData)
 ,cleanup(removeTestData)
]).


test('it should keep atom unchanged') :-
  ground(p, XF)
 ,assertion( XF == p)
 ,ground(p(#n), XF2)
 ,assertion(XF2 == p(#n))
.

test('it should keep formula withouth quantifiers unchanged') :-
  F = ( (avb) & (know(c) => -(cons(d) <= (e <=> f))))
 ,agent:ground(F, F)
.

test('it should expand existential quantifier correctly',[
]) :-
  F = ?([X in testDomain], p(X))
 ,agent:ground(F, XF)
 ,write(XF)
 ,XF = 'v'(p(#a), p(#b), p(#c))
.

%TODO test for nested quantifiers


test('it should expand universal quantifier correctly') :-
  F = !([X in testDomain], p(X))
 ,XF = '&'(p(#a), p(#b), p(#c))
 ,agent:ground(F, XF)
.

:- end_tests(grounding).



:- begin_tests(entailment).

test('should entail single fact in list') :-
  agent:entails([p(#name)], p(#name))
.

test('should entail fact among other facts in list') :-
  agent:entails([a, b, c, p(#name),q], p(#name))
.


test('should entail one fact of a disjunction') :-
  agent:entails([p(#name)], 'v'(a, p(#name), q))
.

test('should entail no fact of a disjunction') :-
  \+ agent:entails([a, b], 'v'(p(#name), q))
.


test('should entail all facts of a conjunction') :-
  agent:entails([a, b, p(#name), q], &(p(#name), q))
.

test('should entail only one fact of a conjunction') :-
  \+ agent:entails([a, b, p(#name)], &(p(#name), q))
.

:- end_tests(entailment).




:- begin_tests(resolveOperation).

test('should entail single fact in list') :-
  agent:resolve([p(#name)], p(#name), true)
.

test('should entail fact among other facts in list') :-
  agent:resolve([a, b, c, p(#name),q], p(#name), true)
.


test('should entail one fact of a disjunction') :-
  agent:resolve([p(#name)], 'v'(a, p(#name), q), true)
.

test('should entail no fact of a disjunction') :-
  agent:resolve([a, b], 'v'(p(#name), q), false)
.


test('should entail all facts of a conjunction') :-
  agent:resolve([a, b, p(#name), q], &(p(#name), q), true)
.

test('should entail only one fact of a conjunction') :-
  agent:resolve([a, b, p(#name)], &(p(#name), q), false)
.

:- end_tests(resolveOperation).
























