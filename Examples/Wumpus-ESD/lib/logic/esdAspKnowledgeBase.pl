:- module(esdAspKnowledgeBase,
  [ openNewKb/1
  , openKb/1
  , closeKb/1
  , addAxiomToKb/2
  , addDefaultToKb/2
  , isEntailedByKb/1
  , isEntailedByAugmentedKb/2
  ]).

:- use_module(
  [ 'esdAspTranslation'
  , 'esdRepresentation'
  , '../asp/clingo'
  , library(gensym) % Generate unique id atoms
  ]).


% This module represents a transparent esd initial knowledge base
% that under the hood is represent by an answer set logic program and
% written to a file.
% We can incrementally add axioms and defaults to the knowledge base and ask if
% propositional queries are entailed.

initKbFileName("initKb.lp").

% Needed for modular esd to asp translation and asp knowledge base update
% topLevelIdAtom(h).


aspKbHeader(Header) :- Header =
  [ comment("This answer set logic program represents an initial esd knowledge base.")
  , comment("It uses the gringo syntax v3 and clasp.")
  , comment("'true' shall be in every answer set")
  , fact(true)
  , comment("hide 'true' from answer set output")
  , hide(true)
  , comment("A answer set is not allowed to include 'false'")
  , constr([false], [])
  % , fact(-false)
  % , hide(-false)
  , comment("Start of knowledge base:")
  ].


aspQueryHeader(Header) :- Header =
  [ comment("This answer set logic program represents a query against a knowledge base.")
  , comment("Invoke it together with the knowledge base.")
  ].


% Create an empty asp file
openNewKb(Stream) :- true
  , initKbFileName(AspFile)
  , open(AspFile, write, Stream)
  , aspKbHeader(AspCompL)
  , forall(member(Comp, AspCompL), writeAspComp(Stream, Comp))
  , retractall(axiomIdAtom(_))
  , retractall(esdKbAxiomCount(_))
  , assertz(esdKbAxiomCount(0))
  .

% Open existing kb to append new axioms
% For performance when adding many axioms after the other
% it's the calling module's responsibility to open and close the Kb.
openKb(Stream) :- true
  , initKbFileName(AspFile)
  , open(AspFile, append, Stream)
  .

closeKb(Stream) :- close(Stream).


% F must be static, objective Esd formula

% We already have 'true' in the knowledge base
addAxiomToKb(_Stream, true) :- !.

addAxiomToKb(Stream, Formula) :- true
  , writeFormulaAsAsp(Stream, Formula)
  .

% addDefaultToKb(Stream, default(KnowFormula, ConsistencyFormula, ConclusionFormula))
addDefaultToKb(Stream, Default) :- !
  , writeDefaultAsAsp(Stream, Default)
  .


isEntailedByKb(Query) :- isEntailedByAugmentedKb([], Query).

% isEntailedByAugmentedKb(AugmentingDefaultL, Query)
% Test if query follows from the knowledge base if we temporarily
% add some augmenting ground default rules to it.
isEntailedByAugmentedKb(_DefaultL, true) :- !. % Is always entailed

isEntailedByAugmentedKb(DefaultL, Query) :- !
  , newQueryFileName(QueryFile)
  , open(QueryFile, write, Stream)
  , aspQueryHeader(AspCompL)
  , forall(member(Comp, AspCompL), writeAspComp(Stream, Comp))
  , forall(member(Default, DefaultL), writeDefaultAsAsp(Stream, Default))
  , negateFormula(Query, NegQuery)
  , writeFormulaAsAsp(Stream, NegQuery)
  , close(Stream)
  , initKbFileName(KbFile)
  , isAspFilesUnsat([KbFile, QueryFile])
  .



newQueryFileName(FileName) :- !
  , gensym(initKbQuery, Name)
  , atomics_to_string([Name, ".lp"], FileName)
  .
