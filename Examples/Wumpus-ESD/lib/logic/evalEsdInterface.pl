:- module(evalEsdInterface,
  [ eval/3
    %System Tools
  , initializeDB/0
  , finalizeDB/0
  , can_roll/1
  , must_roll/1
  , roll_db/2
  , handle_sensing/4
  , debug/3
  , system_action/1
    %For transitioning
  , sensing/2
  , sensed/3
  , inconsistent/1
  , domain/2
  , rdomain/2
  , getdomain/2
  , calc_arg/3
  , before/2
  , assume/4
    %From bat
  , prim_action/1
  , poss/2
  ]).

:- use_module(esdReasoning).


% This is an interface-adapter/wrapper that implements the eval-interface
% required by the IndiGolog transition system and delegates all calls
% to the ESD-Agent which knows how to answer queries to the knowledge base
% and encapsulates/hides the full basic action theory.

% Delegate to the ESD-KB.
initializeDB :- initEsdReasoner.


% No action.
finalizeDB.



% eval/3
% eval(Queryformula, History, BooleanAnswer)
% This is the main predicate by the transition system.
% Evaluate whether queryformula 'QF' follows from the knowledgebase at history 'H'
% and return the boolean answer 'true' in case it does or 'false' otherwise.
eval(neg(QF), H, Answer) :- !,
 % workaround, in if( testcondition)else.. indigolog also asks neg(testcondition) for the else part
  eval(-QF, H, Answer)
.

eval(QF, H, Answer) :-
  toESDHistory(H, ESDH)
% ,nl, print_message(debug(main), 'eval was asked:')
% ,write('Query: '), write(QF), nl
% ,write('History: '), write(ESDH), nl
 ,(askKnown(ESDH, QF) -> Answer = true; Answer = false)
 ,write('QueryResult: '), write(Answer), nl, nl
.


toESDHistory([], []).

% sensingResult was added this way, by having a duplicate action name in
% 'A' and inside 'ActionWithSR'
toESDHistory([_A, esdInternal(ActionWithSR)| SubH], [ActionWithSR |ESDSubH]):- !,
  toESDHistory(SubH, ESDSubH)
.

toESDHistory([A |SubH], ESDH) :-
  toESDHistory(SubH, ESDSubH)
 ,(isPrimitiveAction(A) -> ESDH = [A | ESDSubH]
   ;ESDH = ESDSubH
  ).

% The ESD-Agent doesn't support a knowledgebase progression, so we cannot roll
% the db forward.
can_roll(_History) :- fail.

must_roll(_History) :- fail.

roll_db(History,History). % roll_db doesn't change 'History'



% handle_sensing/4
% handle_sensing(Action, History, SensingResult, ResultingHistory)
% Encode an action 'A' and its 'SensingResult' into history 'H'.
% This is necessary because Indigolog doesn't keep track of the sensing results
% itself, because it doesn't know how we would like to use it.
% We add a new "action" called 'esdInternal', which we declare as a 'system_action'
% so that it is ignored by indigolog interpreter, where we save the
% action 'A' together with its 'SensingResult'. However we must keep
% 'A' as a single element in the History for indigolog the interpreter.
% Note: The 'A' we get was also already added to the History.
handle_sensing(A, [A |SubH], SensingResult, [A, esdInternal((A, SensingResult)) |SubH]) :- !
  , toESDHistory(SubH, EsdH)
  , indiToEsdSensingResult(SensingResult, EsdSR)
  , actionExecuted(EsdH, A, EsdSR)
  .

indiToEsdSensingResult(1, true).
indiToEsdSensingResult(0, false).


% system_action/1 allows us to define actions that are used by us in this
% projector and are probably ignored elsewhere.
system_action(esdInternal(_ActionWithSensingResult)).



% No use here.
debug(_A, _H, _S).



% "action A is a sensing action with a list L of possible outcomes"
% indigolog.pl uses this predicate to determine if an action is
% an sensing action.
sensing(A, _L) :- isSensingAction(A).



% Deprecated??? is never called by IndiGolog!!!
% sensed/3
% sensed(Action, SensingResult, History)
% Notify agent, that action 'A' was executed at history 'H' and returned
% sensing result 'SR'. Simple delegation to esd-interaction operator EXE.
sensed(A, SensingResult, H) :-
  toESDHistory(H, ESDH)
 ,print_message(debug(main), 'What the fuck is going on??? Depreceated Sensing in eval_esd')
 ,write('Action: '), write(A), nl
 ,write('SensingResult: '), write(SensingResult), nl
 ,actionExecuted(ESDH, A, SensingResult)
 ,fail
.



% We assume that the knowledge base is alwasy consistent.
%inconsistent(_H) :- askCons(H, true).
inconsistent(_H) :- fail.



% domain/2: assigns a user-defined domain to a variable.
domain(V, D)  :- getdomain(D, L), member(V, L).
rdomain(V, D) :- getdomain(D, L), shuffle(L,L2), !, member(V, L2).



% "Given a domain D, getdomain/2 returns a list of all items in the domain"
% Get a list 'L' of all standard names that belong to domain 'D'
getdomain(D, L) :- is_list(D) -> L=D; cachedDomain(D, L).



% calc_arg/2 evaluates all arguments of predicate P wrt. history H
% We do nothing, if calc_arg is called!.
% This is used in transfinal.pl to simulate an exogenous action.
calc_arg(P, P, _).



% History H1 is a previous history of H2
before(H1,H2):- append(_,H1,H2).



% "Set fluent F to value V at H, return H1 (add e(F,V) to history H)"
% assume(F,V,H,[e(F,V)|H]).
assume(Fluent, Value, History, _) :-
  print_message(error, "Assumed sth."), nl
 ,write(Fluent), write(Value), write(History),nl
.



prim_action(Action) :- isPrimitiveAction(Action).

poss(Action, poss(Action)). %Action is possible iff. poss(Action) is true.

