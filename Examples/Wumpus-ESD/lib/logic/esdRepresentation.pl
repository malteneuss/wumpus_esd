:- module(esdRepresentation,
 [ isEsdAtom/1
 , isEsdLit/1
 , negateFormula/2
 , replaceVarWithStdName/4
 , simplify/2
 , simplifyRecursive/2
 , listToDisjuncFormula/2
 , listToConjuncFormula/2
 , op(1110, xfx, <=>) % Equivalence 'a <=> b' = '(a=>b) & (b<=a)'
 , op(1100, xfy, => ) % Implication, no "<="
 , op(700, xfy, v)    % Disjunction: 'p v q v r' = '(p v q) v r'
 , op(600, xfy, &)    % Conjunction: 'p & q & r' = 'p & (q & r)'
 , op(500, fy, !)     % Universal Quantifier, no conflict with cut-operator
 , op(500, fy, ?)     % Existential Quantifier
 , op(400, xfy, in)   % Variable-Domain-Connector '?([Var in domain], p(Var))'
 , op(300, fx, #)     % marker for #stdNames
%, op(400, fy, -) % Strong/classical Negation: '-p', already defined
 ]).

:- use_module('../functionalStyle').
:- ensure_loaded('../../../../lib/common').

% Define the logical connectives to construct logical first order formulas in Prolog.
% Some connectives like '-' are built-in to Prolog and we don't want to redefine them
% because this can lead to unexpected errors.

%TODO list valid well-formed-formulas
isStdName(#(_)). % All standard names start with '#'


isEsdAtom(A) :-
  functor(A, Functor, _Arity)
 ,\+ member(Functor, ['-', '&', 'v', '=>', '<=', '<=>', '@', 'know', 'cons', '?', '!'])
.


isEsdLit(-A) :- !, isEsdAtom(A) . % Either an atom

isEsdLit(A) :- isEsdAtom(A). % Or its negation.


negateFormula(-F, F) :- !.
negateFormula(F, -F).



replaceVarWithStdName(Var, F, StdName, FwithStdName) :-
  subv(Var, StdName, F, FwithStdName).

% Create a big disjunction of a list of formulas
listToDisjuncFormula([], false) :- !.
listToDisjuncFormula([true | _RestL], true) :- !.
listToDisjuncFormula([false | RestL], DisjuncF) :- !
  , listToDisjuncFormula(RestL, DisjuncF)
  .
listToDisjuncFormula([SubF | RestL], DisjuncF) :- !
  , listToDisjuncFormula(RestL, RestDisjuncSubF)
  , simplifyDisjunction(SubF v RestDisjuncSubF, DisjuncF)
  .

% Create a big conjunction of a list of formulas
listToConjuncFormula([], true) :- !.
listToConjuncFormula([false | _RestL], false) :- !.
listToConjuncFormula([true | RestL], DisjuncF) :- !
  , listToConjuncFormula(RestL, DisjuncF)
  .
listToConjuncFormula([SubF | RestL], ConjuncF) :- !
  , listToConjuncFormula(RestL, RestConjuncSubF)
  , simplifyConjunction(SubF & RestConjuncSubF, ConjuncF)
  .


simplifyConjunction(F1 & true, F1) :- !.

simplifyConjunction(true & F2, F2) :- !.

simplifyConjunction(_F1 & false, false) :- !.

simplifyConjunction(false & _F2, false) :- !.

simplifyConjunction(F, F) :- !. % Nothing could be simplified

simplifyDisjunction(_F1 v true, true) :- !.

simplifyDisjunction(true v _F2, true) :- !.

simplifyDisjunction(F1 v false, F1) :- !.

simplifyDisjunction(false v F2, F2) :- !.

simplifyDisjunction(F, F) :- !. % Nothing could be simplified

% simplify(Formula, SimplifiedFormula)/2
% Remove unneccessary equalities, disjunctions and conjunctions
% NOTE: this predicate simplifies the top-level logic operator only
% and doesn't go down through the whole formula. Use 'simplifyRecursive' for that.
simplify(true, true) :- !.

simplify(false, false) :- !.

simplify(Term1 = Term2, true) :- Term1 == Term2, !.

simplify(Term1 = Term2, Term1 = Term2) :- (var(Term1) ; var(Term2)), !.

simplify(#Name1 = #Name2, SimpleF) :-
  (Name1 == Name2 ->  SimpleF = true
   ;SimpleF = false
  ), !
.


simplify(-true, false) :- !.

simplify(-false, true) :- !.


simplify(F1 & true, F1) :- !.

simplify(true & F2, F2) :- !.

simplify(_F1 & false, false) :- !.

simplify(false & _F2, false) :- !.


simplify(_F1 v true, true) :- !.

simplify(true v _F2, true) :- !.

simplify(F1 v false, F1) :- !.

simplify(false v F2, F2) :- !.


simplify(_F1 => true, true) :- !.

simplify(true => F2, F2) :- !.

simplify(F1 => false, SimpleF) :- !, simplify(-F1, SimpleF).

simplify(false => _F2, true) :- !.

simplify(F1 => F2, true) :- F1 == F2, !.


simplify(true <=> false, false) :- !.

simplify(false <=> true, false) :- !.

simplify(F1 <=> F2, true) :- F1 == F2, !.


simplify(?(_VariableL, true), true) :- !.

simplify(?(_VariableL, false), false) :- !.

simplify(!(_VariableL, true), true) :- !.

simplify(!(_VariableL, false), false) :- !.


simplify(@(_ActionL, true), true) :- !.

simplify(@(_ActionL, false), false) :- !.


simplify(know(true), true) :- !.

simplify(know(false), false) :- !.

simplify(cons(true), true) :- !.

simplify(cons(false), false) :- !.


simplify(F, F) :- !.



simplifyRecursive(-F1, SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(-SimpleF1, SimpleF)
.

simplifyRecursive(F1 & F2, SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplifyRecursive(F2, SimpleF2)
 ,simplify(SimpleF1 & SimpleF2, SimpleF)
.

simplifyRecursive(F1 v F2, SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplifyRecursive(F2, SimpleF2)
 ,simplify(SimpleF1 v SimpleF2, SimpleF)
.

simplifyRecursive(F1 => F2, SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplifyRecursive(F2, SimpleF2)
 ,simplify(SimpleF1 => SimpleF2, SimpleF)
.

simplifyRecursive(F1 <=> F2, SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplifyRecursive(F2, SimpleF2)
 ,simplify(SimpleF1 <=> SimpleF2, SimpleF)
.

simplifyRecursive(?(VariableL, F1), SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(?(VariableL, SimpleF1), SimpleF)
.

simplifyRecursive(!(VariableL, F1), SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(!(VariableL, SimpleF1), SimpleF)
.

simplifyRecursive(@(ActionL, F1), SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(@(ActionL, SimpleF1), SimpleF)
.

simplifyRecursive(know(F1), SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(know(SimpleF1), SimpleF)
.

simplifyRecursive(cons(F1), SimpleF) :- !,
  simplifyRecursive(F1, SimpleF1)
 ,simplify(cons(SimpleF1), SimpleF)
.

simplifyRecursive(F, SimpleF) :- !, simplify(F, SimpleF).
