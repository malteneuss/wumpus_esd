:- module(esdReasoning,
  [ initEsdReasoner/0
  , askKnown/2
  , actionExecuted/3
   %Helper
  , isPrimitiveAction/1
  , isSensingAction/1
  , isNonSensingAction/1
  , isFluent/1
  , cachedDomain/2
 ]).

:- use_module(
 [ 'esdRepresentation'
 , 'esdAspTranslation'
 , 'esdAspKnowledgeBase'
 , '../functionalStyle'
 , '../asp/clingo'
 , '../../wumpusEsdBat' %TODO Replace explicit dependency by path argument to file which should be loaded dynamically in initEsdReasoner/0
 , library(gensym) % Generate unique identifier atoms
 ]).

:- dynamic(
 [ cachedInitKb/1
 , cachedDefault/1
 , cachedDomain/2
 , extensions/2
 ]).


% We define an abstract knowledge-based agent based on the
% Epistemic Situation Calculus with Defaults (ESD) found in the paper
% 'Efficient Default Reasoning in Knowledge-Based' GOLOG Programs With Answer Set Programming'
%
% NOTE: The format for the action history 'H' in code is as follows:
% 1. The history 'H' is a list of actions, having the most recent action in the front
% 2. All actions are primitive and denoted by a standard name, starting with '#'
% 3. Already executed actions are inside brackets '(#action, SenseResult)'
% 4. Not yet executed actions are single elements in the list.
% e.g.
% [#NewestAction2, #senseAction2, (#executedSenseAction1, false), (#executedAction1, true)]

initEsdReasoner :- !
  , initBat % Generate possible Basic Action Theory Axioms
  , cacheInitKb
  .



% Cache all axioms from the initial knowledge base for faster retrieval
cacheInitKb :- !
  , retractall(cachedDomain(_,_))
  , forall(domain(Name, StdNameL), assertz(cachedDomain(Name, StdNameL)))
  , openNewKb(Stream)
  , retractall(cachedInitKb(_))
  , forall(init(Axiom),
      ( assertz(cachedInitKb(Axiom))
      , ground(Axiom, GroundAxiom)
      , addAxiomToKb(Stream, GroundAxiom)
      ))
  , retractall(cachedDefault(_))
  , forall(default(KnowF, ConsF, ConcF),
      ( ground(KnowF, GroundKnowF)
      , ground(ConsF, GroundConsF)
      , ground(ConcF, GroundConcF)
      , D = default(GroundKnowF, GroundConsF, GroundConcF)
      , assertz(cachedDefault(D))
      , addDefaultToKb(Stream, D)
      ))
  , closeKb(Stream)
  .



% askKnown(ActionHistory, QueryFormula)
% Succeed iff. 'F' is known by our ESD-Agent.
% See Theorem 4.5 from
% 'Default Reasoning in Knowledge-Based' GOLOG Programs With Answer Set Programming'
% If KB is inconsistent, then the answer is meaningless.
askKnown(H, Query) :- true
  , print_message(debug(agent), "Agent was asked:")
  , write("Query: "), write(Query), nl
  , simplifyRecursive(Query, SimpleQuery)
  , regress(H, SimpleQuery, RegQuery)
  , write("Regressed query: "), write(RegQuery), nl
  , ground(RegQuery, GroundQuery) % Expand quantifier and perform CWA replacement
  , write("Ground query: "), write(GroundQuery), nl
  , reduce(GroundQuery, AugmentingDefaultRuleL, ReducedQuery)
  , write("Reduced query: "), write(ReducedQuery), nl
  , isEntailedByAugmentedKb(AugmentingDefaultRuleL, ReducedQuery)
  .



% actionExecuted(OldActionHistory, NewestAction, SenseResult).
% Tell the agent which action was executed with what sensing result
% so that it can add newly gained knowledge to its KB
% Note: SenseResult is either 'true' or 'false'

% Nonsensing action doesn't give new knowledge
actionExecuted(_, A, _) :- isNonSensingAction(A), !.

actionExecuted(H, A, SenseResult) :- true
  , print_message(debug(agent), 'Add knowledge to kb from sensing:')
  , write("Action: "), write(A), nl
  , write("Sensing result: "), write(SenseResult), nl
  , negateFormula(SenseResult, sf(A), SenseF)
  , regress(H, SenseF, RegSenseF)
  , write("Knowledge: "), write(RegSenseF), nl
  , assertz(cachedInitKb(RegSenseF))
  , ground(RegSenseF, GroundRegSenseF) % Grounding only needed for Asp knowledge base
  , openKb(Stream)
  , addAxiomToKb(Stream, GroundRegSenseF)
  , closeKb(Stream)
  .


negateFormula('true', SenseAxiom, SenseAxiom).
negateFormula('false', SenseAxiom, -SenseAxiom).

% regress(ActionHistory, Formula, RegressedFormula)/3
% Remove all actions from a formula F by replacing actions with their right-hand side in
% precondition-, postcondition-, sensing and knowledge axioms.
% To keep track of the many regression rules the order here is equal to order in the paper
% 'Efficient Default Reasoning in Knowledge-Based' GOLOG Programs With Anser Set Programming'

% Nothing to regress on equality of terms (standard names or variables), but they
% have to be identical
regress(_H, Term1 = Term2, Term1 = Term2) :- !.


% Pass through simple logic operators and quantifiers
regress(H, -F1, -RegF1) :- !, regress(H, F1, RegF1).

regress(H, F1 & F2, RegF1 & RegF2) :- !, regress(H, F1, RegF1), regress(H, F2, RegF2).

regress(H, F1 v F2, RegF1 v RegF2) :- !, regress(H, F1, RegF1), regress(H, F2, RegF2).

regress(H, F1 => F2, RegF1 => RegF2) :- !, regress(H, F1, RegF1), regress(H, F2, RegF2).

regress(H, F1 <=> F2, RegF1 <=> RegF2) :- !, regress(H, F1, RegF1), regress(H, F2, RegF2).

regress(H, ?(VariableL, SubF), ?(VariableL, RegSubF)) :- !, regress(H, SubF, RegSubF).

regress(H, !(VariableL, SubF), !(VariableL, RegSubF)) :- !, regress(H, SubF, RegSubF).


% Collect actions from the action operator @([#Action1, #Action2,..], SubF) into the history
regress(H, @(ActionL, SubF), RegF) :- !
  , reverse(ActionL, RevActionL)
  , append(RevActionL, H, NewH)
  , regress(NewH, SubF, RegF)
  .

% Expand poss(#Action) macro by right-hand side of precondition Axiom
regress(H, poss(A), RegF) :- !
  , pre(A, Axiom) % Get precondition axiom for 'A', simplify that axiom and then regress it
  , simplifyRecursive(Axiom, SimpleAxiom)
  , regress(H, SimpleAxiom, RegF)
  .

% Replace sf(#Action) macro by right-hand side of sensing axiom
regress(H, sf(A), RegF) :- !
  , sense(A, Axiom)% Get sensing axiom for 'A', simplify that axiom and then regress it
  , simplifyRecursive(Axiom, SimpleAxiom)
  , regress(H, SimpleAxiom, RegF)
  .

% Regress knowledge operators
% At empty History regress only inside know/cons-formula
regress(H, know(SubF), RegF) :- !, regressKnow(H, know(SubF), RegF).

regress(_H, cons(_SubF), _RegF) :- !, throw("M-regression not yet supported").

% Regress fluents
regress(H, F, RegF) :- isFluent(F), !, regressFluent(H, F, RegF).

% The rest are rigids like 'true', 'false' and don't change
regress(_H, F, F).



% Fluents and epistemic formulas are special in regression

regressKnow([], know(SubF), know(RegSubF)) :- !, regress([], SubF, RegSubF).

% Replace know(SubFormula) with right-hand side of knowledge-theorem.
% Take short-cut with a sense result.
regressKnow([(A, SenseResult) |SubH], know(SubF), RegF) :- !
  , knowledgeActionSenseResultTheorem(SubF, A, SenseResult, Theorem)
  , simplifyRecursive(Theorem, SimpleTheorem)
  , regress(SubH, SimpleTheorem, RegF)
  .

% Take short-cut with nonsensing action
regressKnow([A |SubH], know(SubF), know(RegSubF)) :- isNonSensingAction(A), !
  , regressKnow([A |SubH], SubF, RegSubF)
  .

% Replace know/cons-formula with right-hand side of know/cons-axiom
% See Theorem 3.3 and 3.4 of the paper
% "Default Reasoning in Knowledge-Based Golog Programs with Answer Set Programming".
regressKnow([A |SubH], know(SubF), RegF) :- !
  , knowledgeActionTheorem(SubF, A, Theorem)
  , simplifyRecursive(Theorem, SimpleTheorem)
  , regress(SubH, SimpleTheorem, RegF)
  .


regressFluent([], F, F) :- !.

% SenseResult unimportant
regressFluent([(A, _SenseResult) |SubH], F, RegF) :- !, regressFluent([A |SubH], F, RegF).

regressFluent([A |SubH], F, RegF) :- !
  , post(F, A, Axiom)% Get postcondition axiom for 'A'
  , simplifyRecursive(Axiom, SimpleAxiom)
  , regress(SubH, SimpleAxiom, RegF)
  .

% knowledgeActionSenseResultTheorem(SubFormula, Action, Theorem)
knowledgeActionTheorem(SubF, A, Theorem) :- !
  , Theorem = ( (sf(A) & know(sf(A) => @([A], SubF))) v
                     (-sf(A) & know(-sf(A) => @([A], SubF))) ).

% knowledgeActionSenseResultTheorem(SubFormula, Action, SenseResult, Theorem)
knowledgeActionSenseResultTheorem(SubF, A, true, Theorem) :- !
  , Theorem = (sf(A) & know(sf(A) => @([A], SubF))).

knowledgeActionSenseResultTheorem(SubF, A, false, Theorem) :- !
  , Theorem = (-sf(A) & know(-sf(A) => @([A], SubF))).



% reduce(QueryFormula, AugmentingDefaultRuleL, ReducedQueryFormula)
% Note: Currently only ground queries can be reduced.

reduce(F, _DefaultL, _RedF) :- \+ ground(F), !
  , throw("Reduce nonground formula not yet supported").

% Pass through simple logic operators and quantifiers.
reduce(-SubF, DefaultL, -RedSubF) :- !, reduce(SubF, DefaultL, RedSubF).

reduce(F1 & F2, DefaultL, RedF1 & RedF2) :- !
  , reduce(F1, SubF1DefaultL, RedF1)
  , reduce(F2, SubF2DefaultL, RedF2)
  , append(SubF1DefaultL, SubF2DefaultL, DefaultL)
  .

reduce(F1 v F2, DefaultL, RedF1 v RedF2) :- !
  , reduce(F1, SubF1DefaultL, RedF1)
  , reduce(F2, SubF2DefaultL, RedF2)
  , append(SubF1DefaultL, SubF2DefaultL, DefaultL)
  .

reduce(F1 => F2, DefaultL, RedF1 => RedF2) :- !
  , reduce(F1, SubF1DefaultL, RedF1)
  , reduce(F2, SubF2DefaultL, RedF2)
  , append(SubF1DefaultL, SubF2DefaultL, DefaultL)
  .

reduce(F1 <=> F2, DefaultL, RedF1 <=> RedF2) :- !
  , reduce(F1, SubF1DefaultL, RedF1)
  , reduce(F2, SubF2DefaultL, RedF2)
  , append(SubF1DefaultL, SubF2DefaultL, DefaultL)
  .

reduce(?(_), _DefaultL, ?(_)) :- !, throw("Reduce existenial quantifier not yet supported").

reduce(!(_), _DefaultL, !(_)) :- !, throw("Reduce universal quantifier not yet supported").

% Replace 'know(SubFormala)' by a new unique propositional atom
% and add a default rule according to
% 'Default Reasoning in Knowledge-Based' GOLOG Programs With Answer Set Programming'
reduce(know(SubF), DefaultL, RedSubF) :- !
  , reduce(SubF, SubFDefaultL, RedSubF)
  , KnowFormulaPrefix = knowPhi % New atom is called know_phi in the paper
  , gensym(KnowFormulaPrefix, UniqueAtom) % TODO reuse atom of previous same 'RedSubF'
  , DefaultL = [default(RedSubF, true, UniqueAtom) | SubFDefaultL]
  .

reduce(cons(_SubF), _DefaultL, _RedSubF) :- !, throw("Reduce M-operator not yet supported").

reduce(Atom, [], Atom) :- !.

%Helper predicates

% ground(Formula, GroundedFormulaWithotQuantifier)/1
% Replace quantifiers with all ground instantiations, e.g
% '?([X in rooms], SubF(X))' => exists some 'x' from domain 'rooms'
% expands to the big disjunction 'v(SubF(#room1), SubF(#room2), ..)'
% '!([y in rooms], ..)' => for all 'y' from domain 'rooms'
% expands to the big conjunction '&(SubF(#room1), SubF(#room2), ..)'
% '!/?([x in rooms, y in rooms]', ..) => exists/for all x, y from domain 'rooms'
ground(-SubF, GroundF) :- !
  , ground(SubF, GroundSubF)
  , simplify(-GroundSubF, GroundF)
  .

ground(SubF1 & SubF2, GroundF) :- !
  , ground(SubF1, GroundSubF1)
  , ground(SubF2, GroundSubF2)
  , simplify(GroundSubF1 & GroundSubF2, GroundF)
  .

ground(SubF1 v SubF2, GroundF) :- !
  , ground(SubF1, GroundSubF1)
  , ground(SubF2, GroundSubF2)
  , simplify(GroundSubF1 v GroundSubF2, GroundF)
  .
ground(SubF1 => SubF2, GroundF) :- !
  , ground(SubF1, GroundSubF1)
  , ground(SubF2, GroundSubF2)
  , simplify(GroundSubF1 => GroundSubF2, GroundF)
  .

ground(SubF1 <=> SubF2, GroundF) :- !
  , ground(SubF1, GroundSubF1)
  , ground(SubF2, GroundSubF2)
  , simplify(GroundSubF1 <=> GroundSubF2, GroundF)
  .

ground(know(SubF), GroundF) :- !
  , ground(SubF, GroundSubF)
  , simplify(know(GroundSubF), GroundF)
  .


ground(?(VarL, SubF), GroundF) :- !
  , groundExistQuantifier(?(VarL, SubF), GroundF)
  .

ground(!(VarL, SubF), GroundF) :- !
  , groundUniversalQuantifier(!(VarL, SubF), GroundF)
  .

% CWA simplification only applicable if Atom has no free variables.
ground(Atom, GroundAtom) :- true
  , ground(Atom) % ground/1 checks if 'Atom' has no free variables
  , isAtomWithClosedWorldAssumption(Atom), !
  , (cachedInitKb(Atom) -> % Replace by true if it is in initial Kb
      GroundAtom = true
     ;GroundAtom = false % Otherwise replace by false
    )
  .

ground(Atom, Atom) :- !.

% Expand a list of existenial quantifiers
groundExistQuantifier(?([], SubF), SubF) :- !.

groundExistQuantifier(?([Var in Domain |RestVarL], SubF), GroundF) :- !
  , cachedDomain(Domain, StdNameL)
  , maplist(replaceVarWithStdName(Var, SubF), StdNameL, InstanciatedSubFL)
  , maplist(ground, InstanciatedSubFL, GroundInstanciatedSubFL)
  , listToDisjuncFormula(GroundInstanciatedSubFL, AlmostGroundF)
  , groundExistQuantifier(?(RestVarL, AlmostGroundF), GroundF)
  .

% Expand a list of universal quantifiers
groundUniversalQuantifier(!([], SubF), SubF) :- !.

groundUniversalQuantifier(!([Var in Domain |RestVarL], SubF), GroundF) :- !
  , cachedDomain(Domain, StdNameL)
  , maplist(replaceVarWithStdName(Var, SubF), StdNameL, InstanciatedSubFL)
  , maplist(ground, InstanciatedSubFL, GroundInstanciatedSubFL)
  , listToConjuncFormula(GroundInstanciatedSubFL, AlmostGroundF)
  , groundUniversalQuantifier(!(RestVarL, AlmostGroundF), GroundF)
  .





% isAtomWithClosedWorldAssumption(Atom).
% More readable predicate to check if an atom has/makes the closed world assumption.
% If it does, it still has to be checked against the initial knowledge base whether
% it's true or false
isAtomWithClosedWorldAssumption(Atom) :- cwa(Atom).

isPrimitiveAction(A) :- pre(A, _). %Only primitive actions have precondition axiom.

isSensingAction(A) :-
  isPrimitiveAction(A)
 ,\+ sense(A, true). %Real sensing actions give knowledge other than 'true'.


isNonSensingAction(A) :- sense(A, true).


isFluent(F) :- post(F, _, _). %Only fluents have postcondition axiom.
