:- begin_tests(simplifyFormulas).
:- use_module(esdRepresentation).

test('should leave atom untouched') :-
  simplify(p(#a), Result)
 ,Result == p(#a)
.

test('should leave conjunction of atoms untouched') :-
  simplify(p(#a) & q, Result)
 ,Result == (p(#a) & q)
.

test('should leave disjunction of atoms untouched') :-
  simplify(p(#a) | q, Result)
 ,Result == (p(#a) | q)
.


test('should replace equality of identical standard names with true') :-
  simplify(#a = #a, Result)
 ,Result == true
 ,write(Result)
.

test('should replace equality of different standard names with false') :-
  simplify(#a = #b, Result)
 ,Result == false
.

test('should ignore equality of variable and standard name') :-
  simplify(#a = Variable, Result)
 ,Result == '='(#a,Variable)
.

test('should ignore equality of variables') :-
  simplify(Variable1 = Variable2, Result)
 ,Result == (Variable1 = Variable2)
.

test('should replace conjunction containing false with false') :-
  simplify(&(true, p(#a), q, false), Result)
 ,Result == false
.

test('should replace disjunction containing true with true') :-
  simplify('|'(true, p(#a), q, true), Result)
 ,Result == true
.

test('should replace conjunction containing p and true with p') :-
  simplify(&(true, p(#a)), Result)
 ,Result == p(#a)
.

test('should replace disjunction containing p and false with p') :-
  simplify('|'(false, p(#a)), Result)
 ,Result == p(#a)
.

test('should replace conjunction containing multiple true with true') :-
  simplify(&(true, true, true), Result)
 ,Result == true
.

test('should replace conjunction containing multiple false with false') :-
  simplify('|'(false, false, false), Result)
 ,Result == false
.


test('should replace existential quantifier containing true with true') :-
  simplify(?([_Variable in domain], true), Result)
 ,Result == true
.

test('should replace universal quantifier containing true with true') :-
  simplify(!([_Variable in domain], true), Result)
 ,Result == true
.

test('should replace existential quantifier containing false with false') :-
  simplify(?([_Variable in domain], false), Result)
 ,Result == false
.

test('should replace universal quantifier containing false with false') :-
  simplify(!([_Variable in domain], false), Result)
 ,Result == false
.



:- end_tests(simplifyFormulas).
