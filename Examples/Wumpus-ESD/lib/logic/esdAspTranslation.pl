:- module(esdAspTranslation,
  [ writeFormulaAsAsp/2
  , writeFormulaAsAspWithCustomId/3
  , writeDefaultAsAsp/2
  , writeAspComp/2
  ]).

:- use_module(
  [ 'esdRepresentation'
  , '../asp/aspRepresentation'
  , '../asp/clingo'
  , '../functionalStyle'
  , library(gensym) % Generating new unique atoms
  ]).



% writeFormulaAsAsp/3
% writeFormulaAsAsp(Stream, FormulaIdAtom, EsdFormula)
% Convert any equation-free, quantifier-free, static, objective
% Esd formula(= any plain propositional formula) into
% an answer set program(=set of ASP components)
% whoose stable models are classical models of the formula.
% Taken from the paper "From (quantified) boolean formulae to answer set programming"
% The idea is similar to the famous Tseitin-Encoding of propositional
% formulas in that
% we introduce new helper atoms for each subformula.
% Each helper atom must uniquely identify his subformula, hence is called
% Id Atom.
% For performance any upcoming Asp component is directly written to stream.
writeFormulaAsAsp(Stream, F) :- !
  , gensym(topId, Id) % Generate unique atom to be used as Id for Formula 'F'
  , string_concat("Formula start: ", Id, Comment)
  , writeAspComp(Stream, comment(Comment))
  , writeAspComp(Stream, constr([], [Id])) % top-level Id must be derived
  , writeFormulaAsAspWithCustomId(Stream, Id, F)
  , writeAspComp(Stream, comment("Formula end"))
  .


% For modularity the calling module can provide the first IdAtom for the passed-in formula.
writeFormulaAsAspWithCustomId(Stream, Id, -F1) :- !
  , atomic_list_concat([Id, '_', 0], SubId0)
  , writeAspComp(Stream, hide(Id))
  , writeAspComp(Stream, rule([Id], [], [SubId0])) % h :- not h_0
  , writeFormulaAsAspWithCustomId(Stream, SubId0, F1)
  .

writeFormulaAsAspWithCustomId(Stream, Id, F1 & F2) :- !
  , atomic_list_concat([Id, '_', 0], SubId0)
  , atomic_list_concat([Id, '_', 1], SubId1)
  , writeAspComp(Stream, hide(Id))
  , writeAspComp(Stream, rule([Id], [SubId0, SubId1], [])) % h :- h_0, h_1
  , writeFormulaAsAspWithCustomId(Stream, SubId0, F1)
  , writeFormulaAsAspWithCustomId(Stream, SubId1, F2)
  .

writeFormulaAsAspWithCustomId(Stream, Id, F1 v F2) :- !
  , atomic_list_concat([Id, '_', 0], SubId0)
  , atomic_list_concat([Id, '_', 1], SubId1)
  , writeAspComp(Stream, hide(Id))
  , writeAspComp(Stream, rule([Id], [SubId0], [])) % h :- h_0
  , writeAspComp(Stream, rule([Id], [SubId1], [])) % h :- h_1
  , writeFormulaAsAspWithCustomId(Stream, SubId0, F1)
  , writeFormulaAsAspWithCustomId(Stream, SubId1, F2)
  .

writeFormulaAsAspWithCustomId(Stream, Id, F1 => F2) :- !
  , atomic_list_concat([Id, '_', 0], SubId0)
  , atomic_list_concat([Id, '_', 1], SubId1)
  , writeAspComp(Stream, hide(Id))
  , writeAspComp(Stream, rule([Id], [], [SubId0])) % h :- not h_0
  , writeAspComp(Stream, rule([Id], [SubId1], [])) % h :- h_1
  , writeFormulaAsAspWithCustomId(Stream, SubId0, F1)
  , writeFormulaAsAspWithCustomId(Stream, SubId1, F2)
  .

writeFormulaAsAspWithCustomId(Stream, Id, F1 <=> F2) :- !
  , atomic_list_concat([Id, '_', 0], SubId0)
  , atomic_list_concat([Id, '_', 1], SubId1)
  , writeAspComp(Stream, hide(Id))
  , writeAspComp(Stream, rule([Id], [], [SubId0, SubId1])) % h :- not h_0, not h_1
  , writeAspComp(Stream, rule([Id], [SubId0, SubId1], [])) % h :- h_1, h_1
  , writeFormulaAsAspWithCustomId(Stream, SubId0, F1)
  , writeFormulaAsAspWithCustomId(Stream, SubId1, F2)
  .

% TODO avoid duplicate valuation generation rules for atom appearing multiple times
writeFormulaAsAspWithCustomId(Stream, Id, EsdAtom) :- !
  , literalToAsp(EsdAtom, Atom) % EsdAtom may contain symbol '#', is not allowed in asp, so replace
  , writeAspComp(Stream, hide(Id))
  , foreach(member(Rule,
      [ rule([Id], [Atom], []) % h :- a
      % valuation-generating rules
      , rule([Atom], [], [-Atom]) % a :- not -a
      , rule([-Atom], [], [Atom]) % -a :- not a
      ])
      , writeAspComp(Stream, Rule))
  .


% 'default(a,b,c)' stands for 'know(a) & cons(b) => c'
writeDefaultAsAsp(Stream, default(KnowF, ConsF, ConcF)) :- !
  , writeAspComp(Stream, comment("Default start: "))
  , atomizeFormula(KnowF, KnowEquivL, KnowAtom)
  , atomizeFormula(ConsF, ConsEquivL, ConsAtom)
  , atomizeFormula(ConcF, ConcEquivL, ConcAtom)
  , flatten([KnowEquivL, ConsEquivL, ConcEquivL], NewNeededAxiomL)
  , forall(member(Axiom, NewNeededAxiomL), writeFormulaAsAsp(Axiom))
  , negateFormula(ConsAtom, NegConsAtom)
  , writeAspComp(Stream, rule([ConcAtom], [KnowAtom], [NegConsAtom]))
  , writeAspComp(Stream, comment("Default end"))
  .

% atomizeFormula(Formula, ListWithPossibleSingleEquivalence, AtomizedFormula)

% Literal is atomized enough
atomizeFormula(Lit, [], AspLit) :- isEsdLit(Lit), !, literalToAsp(Lit, AspLit).

atomizeFormula(F, [F <=> Atom], Atom) :- !, gensym(atomized, Atom).




writeAspComp(Stream, Rule) :- aspToString(Rule, String), write(Stream, String).




literalToAsp(-EsdAtom, -AspAtom) :- !, literalToAsp(EsdAtom, AspAtom).

literalToAsp(EsdAtom, AspAtom) :- !,  mapArgs(termToAsp, EsdAtom, AspAtom).

termToAsp(#Name, stdName(Name)) :- !.

termToAsp(Term, Term).


