:- module(clingo,
  [ isAspProgSat/1
  , isAspProgUnsat/1
  , isAspFilesSat/1
  , isAspFilesUnsat/1
  , answerSetsByProgram/3
  , answerSetsByFile/3
  ]).

:- use_module(
  [ 'aspRepresentation'
  , library(apply)
  ]).


% Needs SWI 7.x for its string representation.
% This is a wrapper interface for the Asp solver clingo
% consisting of grounder gringo and solver clasp.
% Note: This module uses an older ASP syntax
% of gringo in version 3.
% All asp rules must conform to that syntax.
% e.g. "a :- -b, not c." is represented by 'rule([a], [-b], [c])'
% 'comment("hello")' becomes "% hello."
% 'hide(a, 0)' becomes "#hide a/0."



% Path to gringo/clingo executables relative to current working directory (=pwd)
pathGringo("./lib/asp/gringo").
pathClasp("./lib/asp/clasp").



% isAspProgSat(AspComponentList)
% Succeed if a list of ASP components has a stable model.
isAspProgSat(AspCompL) :-
  clingoByProg("--verbose=0 --quiet=2", AspCompL, Result)
 ,string_concat(_, "SATISFIABLE", Result) %The last word in the raw 'Result' is either 'SATISFIABLE' or 'UNSATISFIABLE'
.

isAspFilesSat(FileL) :-
  clingoByFiles("--verbose=0 --quiet=2", FileL, Result)
 ,string_concat(_, "SATISFIABLE", Result) %The last word in the raw 'Result' is either 'SATISFIABLE' or 'UNSATISFIABLE'
.


% unsatisfiable/1
isAspProgUnsat(AspCompL) :-
  clingoByProg("--verbose=0 --quiet=2", AspCompL, Result)
 ,string_concat(_, "UNSATISFIABLE", Result) %The last word in the raw 'Result' is either 'SATISFIABLE' or 'UNSATISFIABLE'
.

isAspFilesUnsat(FileL) :-
  clingoByFiles("--verbose=0 --quiet=2", FileL, Result)
 ,string_concat(_, "UNSATISFIABLE", Result) %The last word in the raw 'Result' is either 'SATISFIABLE' or 'UNSATISFIABLE'
.


% computeAnswerSets/3
% computeAnswerSets(Count, AspRulesList, Result)
% Compute the first answer sets/stable models given by 'Count'. If count is '0'
% then compute all stable models.
% 'Result' is either 'satisfiable(..)' with a list of
% stable models, each of which is a list of ground literals,
% e.g. [ [p(a),q(a)], [q(b), -p(q)] ]
% or just 'unsatisfiable'.
answerSetsByFile(Count, FileNameL, Result) :-
  getAnswerSets(clingoByFiles, Count, FileNameL, Result)
.

answerSetsByProgram(Count, FileNameL, Result) :-
  getAnswerSets(clingoByProg, Count, FileNameL, Result)
.

getAnswerSets(ClingoPredicate, Count, FileOrAspCompL, Result) :-
  number_string(Count, CountString)
 ,atomics_to_string(["--models ", CountString, " --verbose=0"], Arguments)
 ,call(ClingoPredicate, Arguments, FileOrAspCompL, ClingoResult)
 ,split_string(ClingoResult, "\n", "", LineL) % Split into string per line
 ,maplist(lineToAnswerSet, LineL, SetL)
 ,last(SetL, [SatOrUnsat]) % Cannot use append/3 here due to retry with false.
 ,(SatOrUnsat == satisfiable ->
    append(Temp, [[SatOrUnsat]], SetL)
   ,Result = satisfiable(Temp)
  ;SatOrUnsat == unsatisfiable -> Result = unsatisfiable
  ;write(SatOrUnsat),print_message(error, "Impossible clingo answer: neither SATISFIABLE nor UNSATISFIABLE"), fail
  )
.

lineToAnswerSet(LineString, Set) :-
  split_string(LineString, " ", "", LiteralStringL) % Split into string per literal
 ,maplist(stringToTerm, LiteralStringL, Set)
.

stringToTerm("SATISFIABLE", satisfiable) :- !. % Otherwise becomes _GXX
stringToTerm("UNSATISFIABLE", unsatisfiable) :- !. % Otherwise becomes _GXX
stringToTerm(String, Term) :- term_string(Term, String).



% Invoke by filenames
clingoByFiles(TerminalArgs, FileNameL, ClingoResultString) :- !
  , atomics_to_string(FileNameL, " ", FilesString)
  , pathGringo(GringoPath)
  , pathClasp(ClaspPath)
  , atomics_to_string(
      [ GringoPath  , " " , FilesString
      , " | " , ClaspPath , " " , TerminalArgs
      ], Command)
  , terminal(Command, ClingoResultRaw)
  , string_concat(ClingoResultString, "\n", ClingoResultRaw) % Remove last newline-symbol
  .

% Invoke by passing answer set program
clingoByProg(AspCompL, TerminalArgs, ClingoResultString) :- !
  , maplist(aspToString, AspCompL, RuleStringL)
  , atomics_to_string(RuleStringL, "\n", AspProgString)
  , pathGringo(GringoPath)
  , pathClasp(ClaspPath)
  , atomics_to_string(
      [ "echo '"
      , AspProgString
      , "' | "
      , GringoPath
      , " | "
      , ClaspPath
      , TerminalArgs
      ], Command)
  , terminal(Command, ClingoResultRaw)
  ,string_concat(ClingoResultString, "\n", ClingoResultRaw) % Remove last newline-symbol
.

% String interface to commandline terminal
terminal(Command, StdOutputResultString) :- !
  , atom_string(CommandAtom, Command)
  , open(pipe(CommandAtom), read, Stream)
  , read_string(Stream, _LineLength, StdOutputResultString)
  , close(Stream)
  .
