:- begin_tests(clingoInvocation).
:- use_module(clingo).

test('should return satisfiable' ) :-
  satisfiable(["a.", "b."])
.

test('should return satisfiable and the single correct answer set') :-
  computeAnswerSets(0, ["a.", "b."], Result)
 ,Result == satifiable([[a, b]])
.

test('should return satisfiable and the two correct answer sets') :-
  computeAnswerSets(0, ["a :- not -a.", "-a :- not a."], Result)
 ,Result == satifiable([[a], [-a]])
.


test('should return unsatisfiable') :-
  unsatisfiable(["a.", ":- a."])
.

test('should return unsatisfiable and no answer set') :-
  computeAnswerSets(0, ["a.", ":- a."], Result)
 ,Result == unsatisfiable
.

test('should tranlate asp rule representation to real asp string') :-
  aspToString(rule([-a, b, c], [-p(b), b, c], [-g(c), a, b]), Result)
 ,Result == "-a, b, c :- -p(b), b, c, not -g(c), not a, not b."
.

:- end_tests(clingoInvocation).
