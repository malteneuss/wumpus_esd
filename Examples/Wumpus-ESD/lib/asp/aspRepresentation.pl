:- module(aspRepresentation,
  [ aspToString/2
  ]).

:- use_module(
  [ '../functionalStyle'
  , library(apply)
  ]).

% We represent answer set program (ASP) components with prolog terms as follows:
% fact                e.g. '-p.', 'q.'       as: 'fact(-p)', 'fact(q)'
% rule                e.g. 'a :- b, not c.'  as: 'rule([a], [b], [c])'
% constraint          e.g. ':- b, not c.'    as: 'constr([b], [c])'
% comment             e.g. '% Hi'            as: 'comment("Hi")'
% hide-atom-directive e.g. '#hide b/0'       as: 'hide(b)', 'hide(b,0)'
% A list of those components is called an ASP program.


% Translate the Prolog representation of answer set programming rule/line into
% real grino/clasp syntax strings.
aspToString(rule([], [], []), "") :- !.

aspToString(rule([Literal |RestL], [], []), RuleString) :- !
  , aspToString(fact(Literal), LiteralString)
  , aspToString(RestL, [], [], RestString)
  , string_concat(LiteralString, RestString, RuleString)
  .

aspToString(rule([], PositiveLiteralL, NegateLiteralL), RuleString) :- !
  , aspToString(constr(PositiveLiteralL, NegateLiteralL), RuleString)
  .

aspToString(rule(ConclusionLiteralL, PositiveLiteralL, NegateLiteralL), RuleString) :- !
  , positiveToString(ConclusionLiteralL, ConclusionString)
  , aspToString(constr(PositiveLiteralL, NegateLiteralL), RestString) % Hack
  , atomics_to_string([ConclusionString, " ", RestString], RuleString)
  .


aspToString(fact(Literal), RuleString) :- !
  , term_string(Literal, Temp)
  , string_concat(Temp, ".\n", RuleString)
  .


aspToString(constr([], []), "") :- !.

aspToString(constr(PositiveLiteralL, []), RuleString) :- !
  , positiveToString(PositiveLiteralL, LiteralLString)
  , atomics_to_string([":- ", LiteralLString, ".\n"], RuleString)
  .

aspToString(constr([], NegateLiteralL), RuleString) :- !
  , negativeToString(NegateLiteralL, LiteralLString)
  , atomics_to_string([":- ", LiteralLString, ".\n"], RuleString)
  .

aspToString(constr(PositiveLiteralL, NegateLiteralL), RuleString) :- !
  , positiveToString(PositiveLiteralL, PositiveString)
  , negativeToString(NegateLiteralL, NegateString)
  , atomics_to_string([":- ", PositiveString, ", ", NegateString, ".\n"], RuleString)
  .

aspToString(hide(Functor, Arity), RuleString) :- !
  , atomics_to_string(["#hide ", Functor, "/", Arity, ".\n"], RuleString)
  .

aspToString(hide(-Atom), RuleString) :- !
  , functor(Atom, Functor, Arity)
  , atomics_to_string(["#hide -", Functor, "/", Arity, ".\n"], RuleString)
  .

aspToString(hide(Atom), RuleString) :- !
  , functor(Atom, Functor, Arity)
  , aspToString(hide(Functor, Arity), RuleString)
  .



aspToString(comment(String), RuleString) :- !
  , atomics_to_string(["% ", String, "\n"], RuleString)
  .


positiveToString(AtomL, AtomString) :- !
  , maplist(term_string, AtomL, AtomStringL)
  , atomics_to_string(AtomStringL, ", ", AtomString)
  .

negativeToString(AtomL, AtomString) :- !
  , maplist(term_string, AtomL, AtomStringL)
  , maplist(string_concat("not "), AtomStringL, NegAtomStringL)
  , atomics_to_string(NegAtomStringL, ", ", AtomString)
  .
