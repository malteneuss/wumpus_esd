:- module(wumpusEsdBat,
  [ initBat/0
  , domain/2
  , init/1
  , default/3
  , cwa/1
  , pre/2
  , post/3
  , sense/2
 ]).

:- use_module(
  [ 'lib/logic/esdRepresentation'
  , 'lib/functionalStyle'
  ]).


% This basic action theory (BAT) defines the sensing-, pre- and postcondition axioms,
% some closed world assumptions for rigids
% as well as the domains for quantified variables and initial knowledge axioms
% for an esd-agent that only knows the cave size and the relations between rooms
% but not where gold, pits and the wumpus are.

initBat. % We don't need any initialization here

% domain/2
% domain(DomainName, ListOfStdNames)
% Define a set of stdNames that can be substituted for quantifiers in formulas
% e.g. '?([R in rooms],..)' Formuala' will substitute variable 'R' in Formula
% by all stdNames from domain 'rooms'.
domain(rooms, L):- findall(R, isRoom(R), L).

domain(dirs, [#up, #right, #down, #left]).



% init/1
% Axioms representing the initial knowledge base (= in the initial situation)
% Agent:
init(agentLoc(#room(1,1))). % Agent starts in lower left corner
init(looking(#right)).      % Agent starts looking to the right
init(visited(#room(1,1))).  % Agent starts in a visited room
init(safe(#room(1,1))).     % Agent starts in a visited room
init(inCave).               % Agent begins in cave
init(hasArrow).             % Agent starts carrying an arrow
init(-hasGold).             % Agent has no gold in the beginning

% Wumpus
init(wumpusAlive). %Wumpus is alive in the beginning
init(?([R in rooms], wumpusLoc(R))). % The wumpus is somewhere
% init(!([R1 in rooms, R2 in rooms], wumpusLoc(R))). % The only wumpus is somewhere

% Relationships about rooms in cave
init(adj(R1, R2)) :- isAdjacent(R1, R2). % Adjacent rooms
init(adj(R1, R2, Dir)) :-isAdjacentInDirection(R1, R2, Dir).
init(sameLine(R1, R2)) :- isOnSameLine(R1, R2).
init(sameLine(R1, R2, Dir)) :- isOnSameLineInDirection(R1, R2, Dir).
init(clockwise(Dir1, Dir2)) :- clockwiseDirections(Dir1, Dir2).

% Relationships about danger and safety
init(!([R in rooms], visited(R) => safe(R))). % Visited rooms are safe
% Safe rooms don't have a pit or the wumpus
init(!([R in rooms], safe(R) <=> -pitLoc(R) & -wumpusLoc(R))).
% A breeze means a pit is in some adjacent room
% No breeze means no pit is in any adjacent room
init(!([R1 in rooms], breeze(R1) <=> ?([R2 in rooms], adj(R1, R2) & pitLoc(R2)))).
% A stench means the wumpus is in some adjacent room
% No stench means the wumpus is not in any adjacent room
init(!([R1 in rooms], stench(R1) <=> ?([R2 in rooms], adj(R1, R2) & wumpusLoc(R2)))).


% default/3
% default(Prerequisite, Justification, Conclusion).
% Reiter-style default rules for the initial knowledge base used in ESD as
% 'know(Prerequisite) & M(Justification) => Conclusion'
default(true, -visited(R), -visited(R)) :- isRoom(R).
default(true, -safe(R), danger(R)) :- isRoom(R). % TODO is 'visited' really necessary?


% cwa/1
% Define rigids (=predicates that don't change through actions)
% for which the closed world assumption should hold.
% Predicates with CWA can only appear as positive atoms
% in 'init/1', otherwise the initial knowledge base may become inconsistent.
% It still has to be checked against the initial knowledge base whether
% it's true or false
cwa(adj(_,_)).
cwa(adj(_,_,_)).
cwa(sameLine(_,_)).
cwa(sameLine(_,_,_)).
cwa(clockwise(_,_)).

% Helper predicates to define initial axioms
caveSize(3).

isRoom(#room(X,Y)) :- caveSize(Size)
  , between(1, Size, X)
  , between(1, Size, Y)
  .

isAdjacent(#room(X1,Y1), #room(X2,Y2)) :-
    isRoom(#room(X1,Y1))
  , isRoom(#room(X2,Y2))
  , abs(X2-X1) =< 1, abs(Y2-Y1) =< 1
  .

isAdjacentInDirection(R1, R2, Dir) :-
    isAdjacent(R1, R2)
  , isOnSameLineInDirection(R1, R2, Dir)
  .

isOnSameLineInDirection(#room(X1,Y1), #room(X2,Y2), #up)    :-
    isRoom(#room(X1,Y1))
  , isRoom(#room(X2,Y2))
  , X1 =:= X2, Y1+1 =< Y2
  .
isOnSameLineInDirection(#room(X1,Y1), #room(X2,Y2), #down)  :-
    isRoom(#room(X1,Y1))
  , isRoom(#room(X2,Y2))
  , X1 =:= X2, Y1-1 >= Y2
  .
isOnSameLineInDirection(#room(X1,Y1), #room(X2,Y2), #right) :-
    isRoom(#room(X1,Y1))
  , isRoom(#room(X2,Y2))
  , X1+1 =< X2, Y1 =:= Y2
  .
isOnSameLineInDirection(#room(X1,Y1), #room(X2,Y2), #left)  :-
    isRoom(#room(X1,Y1))
  , isRoom(#room(X2,Y2))
  , X1-1 >= X2, Y1 =:= Y2
  .

isOnSameLine(R1, R2) :- isOnSameLineInDirection(R1, R2, _).

clockwiseDirections(#up, #right).
clockwiseDirections(#right, #down).
clockwiseDirections(#down, #left).
clockwiseDirections(#left, #up).



% pre/2
% pre(Action, PreconditionFormula)
% Define the precondition Poss-Axiom, i.e.,
% what must be true for an action to be legally or physically possible.
% One axiom for each primitive action.
pre(A, true) :-
  member(A, [#move, #turnRight, #senseBreeze, #senseGlitter, #senseStench]).

pre(#climb, agentLoc(#room(1,1))).

pre(#pickGold, ?([R in rooms], agentLoc(R) & goldLoc(R))).

pre(#shoot, hasArrow).

% TODO can't we write action-instanciated post-condition SSAs?

% post/3
% post(Fluent, Action, EffectAxiomFormula)
% Define the postcondition axioms, i.e.,
% one Successor State Axioms (ssa) per fluent(property that can change).
% 'Formula' is the right-hand side of a ssa and describes how 'Fluent'
% becomes true by 'Action' and how it becomes false.

% Agent is in 'Room2' iff. it movedForward and was in 'Room1' and was looking in the
% direction of 'Room2' and both rooms are adjacent or agent was already in 'Room1'.
post(agentLoc(R2), A, F) :-
  F = (((A = #move) &
        ?([R1 in rooms, D in dirs],
          agentLoc(R1) & looking(D) & adj(R1, R2, D)
         )
        )
       v ( agentLoc(R2) & -(A = #move))
      ).

% Agent visited 'Room2' iff. it movedForward and was in 'Room1' and was looking in the
% direction of 'Room2' and both rooms are adjacent or agent already visited 'Room1'.
post(visited(R2), A, F) :-
  F = (((A = #move) &
        ?([R1 in rooms, D in dirs],
          agentLoc(R1) & looking(D) & adj(R1, R2, D)
         )
        )
       v visited(R2)
      ).

% Agent is looking into 'Direction2' iff. agent turnedRight and was looking into
% 'Direction1' and 'Direction2' is 90 degrees clockwise of 'Direction1' or agent was already looking
% into 'Direction2'.
post(looking(Dir), A, F) :-
  F = (((A = #turnRight) &
        ?([D in dirs],
          looking(D) & clockwise(D, Dir)
         )
        )
       v looking(Dir) & -(A= #turnRight)
      ).

% Agent is carrying arrow iff. it had arrow before and didn't shootArrow.
post(hasArrow, A, hasArrow & -(A = #shoot)).

% Agent is carrying gold iff. it picked up gold or had it before.
post(hasGold, A, F) :-
  F = ( (A = #pickGold)
        v hasGold
      ).

% Wumpus is alive iff. it was before and wasn't shot by the agent.
post(wumpusAlive, A, F) :-
  F = ( wumpusAlive &
        -'v'( (A = #exogWumpusScream)
              ,(A = #shoot) &
               ?([D in dirs, R1 in rooms, R2 in rooms],
                  onSameLine(D, R1, R2)
                  & agentPositition(R1) & looking(D) & wumpusLoc(R2))
             )
       ).


% Wumpus is in 'Room' iff. it was before and didn't die.
%post(wumpusSafe(R), A, wumpusSafe(R) v -
%post(position(#wumpus, R), A, position(#wumpus, R) & -(A = #exogWumpusScream)).

% Gold is in 'Room' iff. it was there before and agent didn't pick it up.
post(goldLoc(R), A, F) :-
  F = &( goldLoc(R)
        ,-(A = #pickGold)
       ).


% Agent is in the cave iff. it was there before and agent didn't climb out of it.
post(inCave, A, F) :-
  F = &( inCave
        ,-(A = #climb))
.


% sense/2
% sense(Action, KnowledgeFormula)
% Define what knowledge is gained from a sensing result for an action
% One axiom for each primitive action
% For non-sensing actions, simply 'sense(#Action, true)'.

% After '#senseStench' the agent knows if the Wumpus must
% be in one of the adjacent rooms or not.
sense(#senseStench, ?([R in rooms], agentLoc(R) & stench(R))).

% After '#senseBreeze' the agent knows if a pit must be in one of the adjacent
% rooms or not.
sense(#senseBreeze, ?([R in rooms], agentLoc(R) & breeze(R))).

% After '#senseGlitter' the agent know if ther is gold in the current room or not.
sense(#senseGlitter, ?([R in rooms], agentLoc(R) & goldLoc(R))).

% Any other (non-sensing) primitive action doesn't give the agent any knowledge.
sense(A, true):- member(A,[#climb, #move, #turnRight, #pickGold, #shoot]).
