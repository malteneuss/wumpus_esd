Load agent and initialize it beforehand with:
prolog -f agent.pl -g initAgent.

askKnown([#(moveForward)], visited(#(room(1,2)))). % Should fail

askKnown([#(moveForward)], visited(#(room(2,1)))). % Should pass

askKnown([#(moveForward)], agentPosition(#(room(2,1)))). % Should pass

askKnown([#(moveForward), #(moveForward)], visited(#(room(2,1)))). % Should pass

askKnown([#(moveForward), #(moveForward)], agentPosition(#(room(2,1)))). % Should fail



askKnown([#(moveForward)], hasArrow). % Should pass

askKnown([#(shootArrow)], hasArrow). % Should fail



askKnown([#(shootArrow)], wumpusPosition(#(room(1,1)))). % Should fail





executed([], #(senseBreeze), true). % Should add pitPosition(#(room(1,2)))|pitPosition(#(room(2,1)))

executed([#(moveForward), #(turnRight),#(turnRight), #(turnRight), #(moveForward)], #(senseBreeze), true). % At position (2,2) should consider all 4 adjacent rooms, but only compute 2^4-1= 15 extesions because (2,1) is already visited and cannot contain the pit

